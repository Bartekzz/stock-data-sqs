package com.bartek.actions;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.*;
import com.bartek.connection.ClientSQS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActionsSQS
{
    private static AmazonSQS amazonSQS;

    public ActionsSQS() {
        // Get SQL Client
        amazonSQS = ClientSQS.getInstanceOfClientSQS().getClient();

        //createQueue(queueName);
        //listQueues();

/*        StockCompanyIntra stockCompanyIntra = new StockCompanyIntra();
        stockCompanyIntra.setSymbol("24STOR");

        StockDataIntra stockDataIntra = new StockDataIntra();
        stockDataIntra.setStockCompanyIntra(stockCompanyIntra);

        sendMessage(SerializationUtil.pojoTtoJson(stockDataIntra), "messageGroup1", queueName);*/
        //sendMessage("Message 2", "messageGroup1", queueName);

/*        List<Message> messages = receiveMessages(queueName);
        for(Message message : messages) {
            StockDataIntra stockDataIntra = SerializationUtil.jsonToPojo(message);
            System.out.println("Stock data symbol: " + stockDataIntra.getStockCompanyIntra().getSymbol());
            //deleteMessages(message.getReceiptHandle(), queueName);
        }*/
        //deleteQueue("MyFifoQueue");
    }

    public static long messagesInQueue(String queueUrl) throws Exception {
        queueUrl = queueUrl + ".fifo";
        // get all the attributes of the queue
        List<String> attributeNames = new ArrayList<String>();
        attributeNames.add("All");
        // list the attributes of the queue we are interested in
        GetQueueAttributesRequest request = new GetQueueAttributesRequest(queueUrl);
        request.setAttributeNames(attributeNames);
        Map<String, String> attributes = amazonSQS.getQueueAttributes(request)
                .getAttributes();
        int messages = Integer.parseInt(attributes
                .get("ApproximateNumberOfMessages"));
        //int messagesNotVisible = Integer.parseInt(attributes
        //        .get("ApproximateNumberOfMessagesNotVisible"));
        return (long) messages;
    }

    public boolean createQueueIfNotExists(String queueName) {

        queueName = queueName + ".fifo";
        System.out.println("Starting queue check/creation: " + queueName);

        boolean queueCreated = false;

        if(!queueExists(queueName))
        {
            try
            {
                // Create a queue
                System.out.println("Creating a new Amazon SQS FIFO queue called " + queueName + "\n");
                final Map<String, String> attributes = new HashMap<String, String>();

                // A FIFO queue must have the FifoQueue attribute set to True
                attributes.put("FifoQueue", "true");
                // If the user doesn't provide a MessageDeduplicationId, generate a MessageDeduplicationId based on the content.
                attributes.put("ContentBasedDeduplication", "true");

                final CreateQueueRequest createQueueRequest = new CreateQueueRequest(queueName)
                        .withAttributes(attributes);
                amazonSQS.createQueue(createQueueRequest).getQueueUrl();

                queueCreated = true;

            }
            catch (AmazonServiceException ase)
            {
                System.out.println("Caught an AmazonServiceException, which means your request made it " +
                        "to Amazon SQS, but was rejected with an error response for some reason.");
                System.out.println("Error Message:    " + ase.getMessage());
                System.out.println("HTTP Status Code: " + ase.getStatusCode());
                System.out.println("AWS Error Code:   " + ase.getErrorCode());
                System.out.println("Error Type:       " + ase.getErrorType());
                System.out.println("Request ID:       " + ase.getRequestId());
            }
            catch (AmazonClientException ace)
            {
                System.out.println("Caught an AmazonClientException, which means the client encountered " +
                        "a serious internal problem while trying to communicate with SQS, such as not " +
                        "being able to access the network.");
                System.out.println("Error Message: " + ace.getMessage());
            }
        }
        else
        {
            System.out.println("Queue does not exist: " + queueName);
        }

        return queueCreated;
    }

    public static boolean queueExists(String queueName) {
        final String queueNameWithSuffix = queueName + ".fifo";
        System.out.println("Checking if queue exists: " + queueNameWithSuffix);

        boolean queueExists = false;
        System.out.println("queueExists 1: " + queueExists);

        try {
            // List queues
            List<String> queues = amazonSQS.listQueues().getQueueUrls();
            queues.forEach(x -> System.out.println(x));
            if(queues.stream().anyMatch(q -> q.contains(queueNameWithSuffix))) {
                System.out.println("Queue exists 2: " + queueNameWithSuffix);
                queueExists = true;
            }

            /*
            for(String queue : queues) {
                if(queue.contains(queueNameWithSuffix)) {
                    System.out.println("Queue exists 2: " + queueNameWithSuffix);
                    queueExists = true;
                    break;
                }
            }
            */

        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it " +
                    "to Amazon SQS, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered " +
                    "a serious internal problem while trying to communicate with SQS, such as not " +
                    "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
        System.out.println("queueExists 3: " + queueExists);
        return queueExists;
    }

    public static List<String> listQueues() {

        List<String> queueList = new ArrayList<>();
        try {
            // List queues
            System.out.println("Listing all queues in your account.\n");
            for (String queueUrl : amazonSQS.listQueues().getQueueUrls()) {
                System.out.println("  QueueUrl: " + queueUrl);
                queueList.add(queueUrl);
            }
            System.out.println();

        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it " +
                    "to Amazon SQS, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered " +
                    "a serious internal problem while trying to communicate with SQS, such as not " +
                    "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }

        return queueList;
    }

    public static void sendMessage(String messageBody, String messageGroup, String queueName) {

        queueName = queueName + ".fifo";
        try {
            // Send a message
            System.out.println("Sending a message to " + queueName + "\n");
            final SendMessageRequest sendMessageRequest = new SendMessageRequest(queueName, messageBody);

            // When you send messages to a FIFO queue, you must provide a non-empty MessageGroupId.
            sendMessageRequest.setMessageGroupId(messageGroup);

            // Uncomment the following to provide the MessageDeduplicationId
            //sendMessageRequest.setMessageDeduplicationId("1");
            final SendMessageResult sendMessageResult = amazonSQS.sendMessage(sendMessageRequest);
            final String sequenceNumber = sendMessageResult.getSequenceNumber();
            final String messageId = sendMessageResult.getMessageId();
            System.out.println("SendMessage succeed with messageId " + messageId + ", sequence number " + sequenceNumber + "\n");

        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it " +
                    "to Amazon SQS, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered " +
                    "a serious internal problem while trying to communicate with SQS, such as not " +
                    "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
    }

    public static List<Message> receiveMessages(String queueName) {

        queueName = queueName + ".fifo";

        List<Message> messages = null;
        try {
            // Receive messages
            System.out.println("Receiving messages from " + queueName + "\n");
            final ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueName);

            // Uncomment the following to provide the ReceiveRequestDeduplicationId
            //receiveMessageRequest.setReceiveRequestAttemptId("1");
            messages = amazonSQS.receiveMessage(receiveMessageRequest).getMessages();
            for (final Message message : messages) {
                System.out.println("Message");
                System.out.println("  MessageId:     " + message.getMessageId());
                System.out.println("  ReceiptHandle: " + message.getReceiptHandle());
                System.out.println("  MD5OfBody:     " + message.getMD5OfBody());
                System.out.println("  Body:          " + message.getBody());
                for (final Map.Entry<String, String> entry : message.getAttributes().entrySet()) {
                    System.out.println("Attribute");
                    System.out.println("  Name:  " + entry.getKey());
                    System.out.println("  Value: " + entry.getValue());
                }
            }
            System.out.println();
            System.out.println();

        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it " +
                    "to Amazon SQS, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered " +
                    "a serious internal problem while trying to communicate with SQS, such as not " +
                    "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }

        return messages;
    }

    public static void deleteMessages(String messageReceiptHandle, String queueName) {

        queueName = queueName + ".fifo";
        try {
            // Delete the message
            System.out.println("Deleting the message: " + messageReceiptHandle + "\n");
            amazonSQS.deleteMessage(new DeleteMessageRequest(queueName, messageReceiptHandle));

        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it " +
                    "to Amazon SQS, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered " +
                    "a serious internal problem while trying to communicate with SQS, such as not " +
                    "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
    }

    public static void deleteQueue(String queueName) {

        queueName = queueName + ".fifo";
        try {

            // Delete a queue
            System.out.println("Deleting queue: " + queueName + "\n");
            amazonSQS.deleteQueue(new DeleteQueueRequest(queueName));
        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it " +
                    "to Amazon SQS, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered " +
                    "a serious internal problem while trying to communicate with SQS, such as not " +
                    "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
    }

    public static void main(String[] args) {
        System.out.println("ActionSQS main method");
        ActionsSQS actionSQS = new ActionsSQS();
        actionSQS.queueExists("nasdaqomx_2020-06-23");
    }
}
