package com.bartek.connection;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;

public class ClientSQS
{

    private static ClientSQS instanceOfClientSQS = null;

    // static method to create instance of class SQSClient
    public static ClientSQS getInstanceOfClientSQS()
    {
        if (instanceOfClientSQS == null)
            instanceOfClientSQS = new ClientSQS();

        return instanceOfClientSQS;
    }

    /**
     * Returns a client instance for AWS SQS.
     *
     * @return a client that talks to SQS
     */
    public AmazonSQS getClient()
    {
        BasicAWSCredentials credentials;
        try
        {
            credentials = new BasicAWSCredentials("AKIATUMNL3BMNB57GTN5", "hcs0WjqEGADcvp6wcVpqGh2a1Rrx8y95MpPLQbBr");
        }
        catch (Exception e)
        {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                            "Please make sure that your credentials file is at the correct " +
                            "location (~/.aws/credentials), and is in valid format.",
                    e);
        }

        AmazonSQS sqs = AmazonSQSClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.EU_WEST_2)
                .build();

        return sqs;
    }
}
